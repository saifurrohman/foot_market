part of 'pages.dart';

class FootPage extends StatefulWidget {
  @override
  _FootPageState createState() => _FootPageState();
}

class _FootPageState extends State<FootPage> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Column(
          children: [
            // header
            Container(
              padding: EdgeInsets.symmetric(horizontal: defaultMargin),
              color: Colors.white,
              height: 100,
              width: double.infinity,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Foot Market',
                        style: blackFontsStyle1,
                      ),
                      Text(
                        "Let's get some foods",
                        style:
                            greyFontStyle.copyWith(fontWeight: FontWeight.w300),
                      ),
                    ],
                  ),
                  Container(
                    width: 50,
                    height: 50,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        image: DecorationImage(
                            image: NetworkImage(
                        'https://img2.pngio.com/coffee-shop-png-picture-535883-cafe-clipart-junk-shop-cafe-shop-png-512_512.png'),
                            fit: BoxFit.cover)),
                  )
                ],
              ),
            )
            //list of food
            //list of foot tabs
          ],
        )
      ],
    );
  }
}
